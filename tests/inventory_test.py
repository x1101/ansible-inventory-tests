# Testing framework
import pytest
import pytest_ansible

# Ansible Imports
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager

dl = DataLoader()
im = InventoryManager(loader=dl, sources=['inventories/demo_inventory.ini'])
vm = VariableManager(loader=dl, inventory=im)

hosts = im.get_hosts()

# Ensure we don't have an empty inventory
def test_inventory():
    assert len(hosts) != 0

# Check to ensure that host are in one and only one domain group
@pytest.mark.parametrize("host", hosts)
def test_domain(host):
    assert sum('00' in s for s in vm.get_vars(play=None, host=host)['group_names']) == 1, \
        f"{host} in wrong number of Domain Groups"

# Check to ensure that host are in one and only one DataCenter group
@pytest.mark.parametrize("host", hosts)
def test_DC(host):
    assert sum('01' in s for s in vm.get_vars(play=None, host=host)['group_names']) == 1, \
        f"{host} in wrong number of DataCenter Groups"


# Check to ensure that host are in one and only one Cluster group
@pytest.mark.parametrize("host", hosts)
def test_cluster(host):
    assert sum('02' in s for s in vm.get_vars(play=None, host=host)['group_names']) == 1, \
        f"{host} in wrong number of Cluster Groups"

# Check to ensure that host are in one and only one OS group
@pytest.mark.parametrize("host", hosts)
def test_OS(host):
    assert sum('03' in s for s in vm.get_vars(play=None, host=host)['group_names']) == 1, \
        f"{host} in wrong number of OS Groups"
