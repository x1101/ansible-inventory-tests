import pytest  # Testing framework
import os.path  # For opening files
import yaml  # for parsing yaml
from pytest_schema import schema

# Ansible Imports
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader

# Set the tooling
dl = DataLoader()
im = InventoryManager(loader=dl, sources=["inventories/demo_inventory.ini"])
GroupVars = "group_vars/"

# Get all the groups
groups = im.list_groups()

# Prune out the 'built in' ones
groups.remove("all")
groups.remove("ungrouped")

# Define the schema expected for group_vars
group_schema = {
    "vlan": int,
    "cpus": int,
    "cores": int,
    "ram": int,
    "vmdisks": {
        "cluster": str,
        "size": int,
        "type": str,
    },
}


# Ensure expected group_vars files exist
@pytest.mark.parametrize("group", groups)
def test_groupvars_exist(group):
    filepath = GroupVars + group + ".yml"
    filepathfull = GroupVars + group + ".yaml"
    assert os.path.isfile(filepath) or os.path.isfile(
        filepathfull
    ), f"group_vars/{group}.y[a]ml does not exist"


# Ensure that group-vars files match expected schema
@pytest.mark.parametrize("group", groups)
def test_group_schema(group):
    filepath = GroupVars + group + ".yml"
    filepathfull = GroupVars + group + ".yaml"
    try:
        with open(filepath) or open(filepathfull) as yml:
            yamlfile = yaml.safe_load(yml)
    except FileNotFoundError:
        return
    else:
        assert schema(group_schema) == yamlfile
