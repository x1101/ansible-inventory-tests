# Testing framework
import pytest
import pytest_ansible

# Ansible Imports
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager

dl = DataLoader()
im = InventoryManager(loader=dl, sources=['inventories/demo_inventory.ini'])
vm = VariableManager(loader=dl, inventory=im)

hosts = im.get_hosts()

@pytest.mark.parametrize("host", hosts)
def test_ip(host):
    try:
        assert vm.get_vars(host=host)['ip'] is not None, \
            f"{host} has no IP defined"
    except KeyError:
        assert False, \
            f"{host} has no IP defined"


@pytest.mark.parametrize("host", hosts)
def test_build_group(host):
    if '03OS2' in vm.get_vars(host=host)['group_names']:
        try:
            assert vm.get_vars(host=host)['build_type'] is not None, \
                f"{host} is in Windows group but has no build_type set"
        except KeyError:
            assert False, \
                f"{host} is in Windows group but has no build_type set"
