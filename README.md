# Ansible Inventory Testing tools
An attempt at a toolkit to run sanity tests on ansible inventories

## Tools Used
- [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) - for environment management
- [poetry](https://python-poetry.org/) - for simple dependency management
- [ansible-core](https://docs.ansible.com/core.html) - for basic ansible interactoins
- [pytest](https://pytest.org/en/7.4.x/index.html) - for testing framework
  - [pytest-ansible](https://pypi.org/project/pytest-ansible/) - Ansible/PyTest Integrations 

## Setup
- creeate a virtual environment
  - This is optional, but I've found it effective
  - `mkvirtualenv verify_ansible_inventory`
  - `workon verify_ansible_inventory`
- clone repo
- install dependencies
  - `poetry install`
- run test suite
  - `pytest`
